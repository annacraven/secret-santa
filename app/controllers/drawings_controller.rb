class DrawingsController < ApplicationController
  before_action :set_drawing, only: [:show, :destroy, :redraw]
  def index
    @current_year = Time.current.year
    @drawings = Drawing.all.order('year ASC')
  end

  def create
    @drawing = Drawing.new(drawing_params)
    do_drawing
    if @drawing.save
      redirect_to drawings_path, notice: 'Successfully created drawing!'
    else
      redirect_to drawings_path, notice: @drawing.errors.full_messages.first
    end

  end

  def show; end

  def destroy
    @drawing.destroy
    redirect_to drawings_path, notice: 'Drawing destroyed'
  end

  def redraw
    params[:year] = @drawing.year
    @drawing.destroy
    create
  end

  private

    def drawing_params
      params.permit(:year)
    end

    def set_drawing
      @drawing = Drawing.find(params[:id])
    end

    def do_drawing
      selections = Person.all.to_a
      valid = false
      while not valid
        selections.shuffle!
        valid = isValid(selections)
      end
      selections.each_with_index do |presenter, i|
        Selection.create(presenter: presenter, 
          presentee: selections[(i+1) % selections.length], drawing: @drawing)
      end
    end

    # given an array of people, checks if the ordering is valid
    def isValid(people)
      people.each_with_index do |presenter, i| 
        if presenter.last_name == people[(i + 1) % people.length].last_name
          return false
        end
      end
      return true
    end
end