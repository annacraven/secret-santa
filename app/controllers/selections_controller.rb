class SelectionsController < ApplicationController
  # def index
  #   @selections = Selection.all
  #   # TODO make sure this works
  #   @selections.joins(:people).order('presenter.id DESC')
  # end

  # def create
  #   @selection = Selection.new(selection_params)
  #   if not @selection.save
  #     flash['notice'] = 'Failed to create selection'
  #   end
  # end

  private

    def selection_params
      params.require(:selection).permit(:presenter, :presentee)
    end

end