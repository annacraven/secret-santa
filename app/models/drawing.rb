class Drawing < ApplicationRecord
  validates :year, presence: true, uniqueness: true

  has_many :selections, dependent: :destroy
end
