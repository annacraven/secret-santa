class Selection < ApplicationRecord
  belongs_to :drawing
  belongs_to :presenter, class_name: 'Person', foreign_key: 'presenter_id'
  belongs_to :presentee, class_name: 'Person', foreign_key: 'presentee_id'

  validates :presenter, presence: true
  validates :presenter, presence: true
end
