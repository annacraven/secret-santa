class AddOnePersonToSelection < ActiveRecord::Migration[5.2]
  def change
    add_reference :selections, :person, foreign_key: true
  end
end
