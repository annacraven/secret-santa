class AddPresenteeRef < ActiveRecord::Migration[5.2]
  def change
    add_reference :selections, :presentee, foreign_key: true
    remove_reference :selections, :person, foreign_key: true
  end
end
