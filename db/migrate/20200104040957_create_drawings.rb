class CreateDrawings < ActiveRecord::Migration[5.2]
  def change
    create_table :drawings do |t|
      t.integer :year, unique: true

      t.timestamps
    end
  end
end
