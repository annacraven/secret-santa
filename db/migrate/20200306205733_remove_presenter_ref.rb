class RemovePresenterRef < ActiveRecord::Migration[5.2]
  def change
    remove_reference :selections, :presenter, foreign_key: true
    remove_reference :selections, :presentee, foreign_key: true
  end
end
