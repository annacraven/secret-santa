class CreateSelections < ActiveRecord::Migration[5.2]
  def change
    create_table :selections do |t|
      t.references :presenter, foreign_key: true
      t.references :presentee, foreign_key: true

      t.timestamps
    end
  end
end
