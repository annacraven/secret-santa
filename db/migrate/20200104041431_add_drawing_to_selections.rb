class AddDrawingToSelections < ActiveRecord::Migration[5.2]
  def change
    add_reference :selections, :drawing, foreign_key: true
  end
end
