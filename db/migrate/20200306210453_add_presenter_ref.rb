class AddPresenterRef < ActiveRecord::Migration[5.2]
  def change
    add_reference :selections, :presenter, foreign_key: true
  end
end
