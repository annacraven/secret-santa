Rails.application.routes.draw do
  root to: 'dashboards#index'

  resources :people
  resources :drawings, only: [:index, :create, :show, :destroy]
  get 'drawing/:id/redraw', to: 'drawings#redraw', as: 'redraw'
end
